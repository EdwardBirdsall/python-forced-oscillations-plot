import numpy as np
import matplotlib.pyplot as plt


# define function for power lorentzian
def power_lorenzian(x, Fm, omega0, gamma):
    numerator = x**2.0 * Fm**2.0
    denominator = (x**2.0 - omega0**2.0)**2.0 + gamma**2.0 * x**2.0
    return (numerator / denominator)


# create x data for function (to plot)
x = np.linspace(14.0, 19.0, 1000) # min, max, number of points
# enter parameters
Fm = 7.0
omega0 = 17.0
gamma = 0.3
# create y data for function (to plot)
y = power_lorenzian(x, Fm, omega0, gamma) # edit parameter values here
# plot function
plt.plot(x, y, '-')

# load data from csv file
data = np.loadtxt(open("data.csv", "r"), delimiter=",")
# extract x, y, error data
x_data = data[:,0]
y_data = data[:,1] # colon = "all of"
e_data = data[:,2]
# plot data
plt.errorbar(x_data, y_data, yerr=e_data, fmt='_')

# set labels
plt.xlabel('Frequency [Hz]')
plt.ylabel('Power [mV^2]')

# draw and save, then display, figure
plt.draw()
plt.savefig('figure_1.png')
plt.show()

# compute chi-square value
chi2 = (((y_data - power_lorenzian(x_data, Fm, omega0, gamma)) / e_data)**2.0).sum()
ndf = len(y_data) - 3
print (chi2 / ndf)